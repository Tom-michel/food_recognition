// import 'package:flutter/material.dart';
// import 'package:food_recognition/my_home_page.dart';
// import 'package:splashscreen/splashscreen.dart';

// class MySplashScreen extends StatefulWidget {
//   const MySplashScreen({super.key});

//   @override
//   State<MySplashScreen> createState() => _MySplashScreenState();
// }

// class _MySplashScreenState extends State<MySplashScreen> {
//   @override
//   Widget build(BuildContext context) {
//     return SplashScreen(
//       backgroundColor: Colors.green,
//       title: const Text('Food Recognition'),
//       seconds: 5,
//       navigateAfterSeconds: const MyHomePage(title: 'Food Recognition'),
//       useLoader: true,
//       loaderColor: Colors.white,
//       loadingText: const Text("loading", style: TextStyle(color: Colors.white)),
//     );
//   }
// }
